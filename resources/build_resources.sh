#! /bin/bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

${CURRENT_DIR}/app/build_application_resources.sh
${CURRENT_DIR}/ui/build_ui_resources.sh
${CURRENT_DIR}/tts/generate_sounds.sh
