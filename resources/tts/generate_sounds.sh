#! /bin/bash

# Check dependencies
command -v pico2wave >/dev/null 2>&1 || {
  echo >&2 "I require pico2wave (libttspico-utils) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "$(dirname "${CURRENT_DIR}")")"

OUTPUT_BASE_FOLDER="${BASE_DIR}/assets/voices"
mkdir -p "${OUTPUT_BASE_FOLDER}"

function generate_sound_file() {
  LANG="$1"
  FILE="$2"
  TEXT="$3"

  LANG_CODE="$(echo "${LANG}" | cut -d"-" -f1)"
  OUTPUT_FILE="${OUTPUT_BASE_FOLDER}/${LANG_CODE}-${FILE}.wav"

  echo "(${LANG_CODE}) \"${TEXT}\" -> ${OUTPUT_FILE}"
  pico2wave -l "${LANG}" -w "${OUTPUT_FILE}" "${TEXT}"
}

generate_sound_file "fr-FR" "left-hand-on-blue" "main gauche sur le bleu"
generate_sound_file "fr-FR" "right-hand-on-blue" "main droite sur le bleu"
generate_sound_file "fr-FR" "left-foot-on-blue" "pied gauche sur le bleu"
generate_sound_file "fr-FR" "right-foot-on-blue" "pied droit sur le bleu"

generate_sound_file "fr-FR" "left-hand-on-green" "main gauche sur le vert"
generate_sound_file "fr-FR" "right-hand-on-green" "main droite sur le vert"
generate_sound_file "fr-FR" "left-foot-on-green" "pied gauche sur le vert"
generate_sound_file "fr-FR" "right-foot-on-green" "pied droit sur le vert"

generate_sound_file "fr-FR" "left-hand-on-red" "main gauche sur le rouge"
generate_sound_file "fr-FR" "right-hand-on-red" "main droite sur le rouge"
generate_sound_file "fr-FR" "left-foot-on-red" "pied gauche sur le rouge"
generate_sound_file "fr-FR" "right-foot-on-red" "pied droit sur le rouge"

generate_sound_file "fr-FR" "left-hand-on-yellow" "main gauche sur le jaune"
generate_sound_file "fr-FR" "right-hand-on-yellow" "main droite sur le jaune"
generate_sound_file "fr-FR" "left-foot-on-yellow" "pied gauche sur le jaune"
generate_sound_file "fr-FR" "right-foot-on-yellow" "pied droit sur le jaune"

generate_sound_file "en-GB" "left-hand-on-blue" "left hand on blue"
generate_sound_file "en-GB" "right-hand-on-blue" "right hand on blue"
generate_sound_file "en-GB" "left-foot-on-blue" "left foot on blue"
generate_sound_file "en-GB" "right-foot-on-blue" "right foot on blue"

generate_sound_file "en-GB" "left-hand-on-green" "left hand on green"
generate_sound_file "en-GB" "right-hand-on-green" "right hand on green"
generate_sound_file "en-GB" "left-foot-on-green" "left foot on green"
generate_sound_file "en-GB" "right-foot-on-green" "right foot on green"

generate_sound_file "en-GB" "left-hand-on-red" "left hand on red"
generate_sound_file "en-GB" "right-hand-on-red" "right hand on red"
generate_sound_file "en-GB" "left-foot-on-red" "left foot on red"
generate_sound_file "en-GB" "right-foot-on-red" "right foot on red"

generate_sound_file "en-GB" "left-hand-on-yellow" "left hand on yellow"
generate_sound_file "en-GB" "right-hand-on-yellow" "right hand on yellow"
generate_sound_file "en-GB" "left-foot-on-yellow" "left foot on yellow"
generate_sound_file "en-GB" "right-foot-on-yellow" "right foot on yellow"
