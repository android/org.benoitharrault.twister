part of 'activity_cubit.dart';

@immutable
class ActivityState extends Equatable {
  const ActivityState({
    required this.currentActivity,
  });

  final Activity currentActivity;

  @override
  List<dynamic> get props => <dynamic>[
        currentActivity,
      ];
}
