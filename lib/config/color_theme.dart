import 'package:flutter/material.dart';

class TwisterColors {
  static const Color grey = Color.fromARGB(255, 0xF0, 0xE2, 0xE7); // F0E2E7

  static const Color blue = Color.fromARGB(255, 0x3F, 0x84, 0xE5); // 3F84E5
  static const Color green = Color.fromARGB(255, 0x1D, 0xA2, 0x3C); // 1DA23C
  static const Color red = Color.fromARGB(255, 0xE0, 0x0D, 0x3B); // E00D3B
  static const Color yellow = Color.fromARGB(255, 0xE8, 0xD9, 0x17); // E8D917
}
