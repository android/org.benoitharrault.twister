import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:twister/cubit/activity/activity_cubit.dart';

class GameMovesCountWidget extends StatelessWidget {
  const GameMovesCountWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        return OutlinedText(
          text: activityState.currentActivity.history.length.toString(),
          fontSize: 50,
          textColor: Colors.grey,
          outlineColor: Colors.grey.shade200,
        );
      },
    );
  }
}
