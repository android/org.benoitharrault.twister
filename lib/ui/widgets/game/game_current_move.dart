import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:twister/cubit/activity/activity_cubit.dart';
import 'package:twister/models/activity/move.dart';
import 'package:twister/ui/widgets/game/move.dart';

class GameCurrentMoveWidget extends StatefulWidget {
  const GameCurrentMoveWidget({super.key});

  @override
  State<GameCurrentMoveWidget> createState() => _GameCurrentMoveWidgetState();
}

class _GameCurrentMoveWidgetState extends State<GameCurrentMoveWidget> {
  Move? shuffledMove;

  void animate() {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);
    if (activityCubit.state.currentActivity.animationInProgress == true) {
      return;
    }

    const interval = Duration(milliseconds: 200);
    int iterationsLeft = 10;

    activityCubit.setAnimationIsRunning(true);
    shuffledMove = null;

    setState(() {});
    Timer.periodic(
      interval,
      (Timer timer) {
        // animate with random move
        if (iterationsLeft > 1) {
          shuffledMove = Move.pickRandom();
        }

        // last iteration => clear
        if (iterationsLeft == 1) {
          shuffledMove = null;
        }

        // end: pick and keep random move
        if (iterationsLeft == 0) {
          shuffledMove = null;
          timer.cancel();

          activityCubit.pickNewMove();
          activityCubit.setAnimationIsRunning(false);
        }

        setState(() {});
        iterationsLeft--;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        return GestureDetector(
          child: activityState.currentActivity.animationInProgress
              ? Transform.rotate(
                  angle: 2 * pi * Random().nextDouble(),
                  child: MoveWidget(move: shuffledMove ?? Move.createEmpty()),
                )
              : MoveWidget(move: activityState.currentActivity.move ?? Move.createEmpty()),
          onTap: () {
            animate();
          },
        );
      },
    );
  }
}
