import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:twister/cubit/activity/activity_cubit.dart';
import 'package:twister/models/activity/move.dart';
import 'package:twister/models/activity/twister_color.dart';
import 'package:twister/models/activity/twister_member.dart';
import 'package:twister/ui/widgets/game/move.dart';

class GameMovesHistoryWidget extends StatelessWidget {
  const GameMovesHistoryWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        return GestureDetector(
          child: currentGlobalState(activityState.currentActivity.history),
          onTap: () {
            BlocProvider.of<ActivityCubit>(context).deleteHistory();
          },
        );
      },
    );
  }

  Widget currentGlobalState(List<Move>? history) {
    Map<String, TwisterColor?> currentState = {};

    history?.forEach((move) {
      currentState[move.member.toString()] = move.color;
    });

    TwisterMember leftHand = TwisterMember(value: TwisterAllowedMembers.leftHand);
    TwisterMember rightHand = TwisterMember(value: TwisterAllowedMembers.rightHand);
    TwisterMember leftFoot = TwisterMember(value: TwisterAllowedMembers.leftFoot);
    TwisterMember rightFoot = TwisterMember(value: TwisterAllowedMembers.rightFoot);

    Move leftHandMove = Move.createFrom(
      member: leftHand,
      color: currentState[leftHand.toString()],
    );
    Move rightHandMove = Move.createFrom(
      member: rightHand,
      color: currentState[rightHand.toString()],
    );
    Move leftFootMove = Move.createFrom(
      member: leftFoot,
      color: currentState[leftFoot.toString()],
    );
    Move rightFootMove = Move.createFrom(
      member: rightFoot,
      color: currentState[rightFoot.toString()],
    );

    return Padding(
      padding: const EdgeInsets.only(
        left: 30,
        right: 30,
      ),
      child: Table(
        children: [
          TableRow(
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: MoveWidget(move: leftHandMove),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: MoveWidget(move: rightHandMove),
              ),
            ],
          ),
          TableRow(
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: MoveWidget(move: leftFootMove),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: MoveWidget(move: rightFootMove),
              ),
            ],
          )
        ],
      ),
    );
  }
}
