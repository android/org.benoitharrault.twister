import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:twister/config/color_theme.dart';
import 'package:twister/models/activity/move.dart';
import 'package:twister/models/activity/twister_color.dart';
import 'package:twister/models/activity/twister_member.dart';

class MoveWidget extends StatelessWidget {
  const MoveWidget({super.key, required this.move});

  final Move move;

  Color getColor(Move move) {
    switch (move.color?.value) {
      case TwisterAllowedColors.blue:
        return TwisterColors.blue;
      case TwisterAllowedColors.green:
        return TwisterColors.green;
      case TwisterAllowedColors.red:
        return TwisterColors.red;
      case TwisterAllowedColors.yellow:
        return TwisterColors.yellow;
      default:
        return TwisterColors.grey;
    }
  }

  Widget getTextWidget(Move move) {
    TextStyle style = const TextStyle(
      color: Colors.black,
      fontSize: 30,
      fontWeight: FontWeight.bold,
    );

    switch (move.member?.value) {
      case TwisterAllowedMembers.leftHand:
        return Text(tr('left_hand'), style: style);
      case TwisterAllowedMembers.rightHand:
        return Text(tr('right_hand'), style: style);
      case TwisterAllowedMembers.leftFoot:
        return Text(tr('left_foot'), style: style);
      case TwisterAllowedMembers.rightFoot:
        return Text(tr('right_foot'), style: style);
      default:
        return Text('?', style: style);
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final double maxWidth = constraints.maxWidth;
        Color color = getColor(move);

        double containerSize = maxWidth * 0.8;

        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 200),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(scale: animation, child: child);
          },
          child: Container(
            width: containerSize,
            height: containerSize,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(Radius.circular(containerSize)),
              border: Border.all(
                color: color.darken(15),
                width: 15,
              ),
            ),
            child: Image.asset('assets/ui/move-${move.member?.toString() ?? 'blank'}.png'),
          ),
        );
      },
    );
  }
}
