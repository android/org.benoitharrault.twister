import 'package:flutter/material.dart';

import 'package:twister/ui/widgets/game/game_current_move.dart';
import 'package:twister/ui/widgets/game/game_moves_count.dart';
import 'package:twister/ui/widgets/game/game_moves_history.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GameCurrentMoveWidget(),
        GameMovesCountWidget(),
        GameMovesHistoryWidget(),
      ],
    );
  }
}
