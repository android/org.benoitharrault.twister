import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:twister/models/activity/twister_color.dart';
import 'package:twister/models/activity/twister_member.dart';

class Move {
  final TwisterColor? color;
  final TwisterMember? member;

  Move({
    required this.color,
    required this.member,
  });

  factory Move.createEmpty() {
    return Move(
      color: null,
      member: null,
    );
  }

  factory Move.createFrom({TwisterMember? member, TwisterColor? color}) {
    return Move(
      color: color,
      member: member,
    );
  }

  factory Move.pickRandom() {
    return Move(
      color: TwisterColor.pickRandom(),
      member: TwisterMember.pickRandom(),
    );
  }

  @override
  String toString() {
    return '$Move(${toJson()})';
  }

  String toSoundAsset() {
    String base = 'voices/${tr('lang_prefix')}-';
    String member = this.member.toString();
    String color = this.color.toString();

    return '$base$member-on-$color.wav';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'color': color.toString(),
      'member': member.toString(),
    };
  }
}
