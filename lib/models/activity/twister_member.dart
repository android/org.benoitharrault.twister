import 'dart:math';

enum TwisterAllowedMembers {
  leftHand,
  rightHand,
  leftFoot,
  rightFoot,
}

class TwisterMember {
  final TwisterAllowedMembers value;

  TwisterMember({
    required this.value,
  });

  factory TwisterMember.pickRandom() {
    int random = Random().nextInt(TwisterAllowedMembers.values.length);
    return TwisterMember(value: TwisterAllowedMembers.values[random]);
  }

  @override
  String toString() {
    List<String> parts = value.toString().split('.');
    final exp = RegExp('(?<=[a-z])[A-Z]');
    return parts[1].replaceAllMapped(exp, (m) => '-${m.group(0)}').toLowerCase();
  }
}
