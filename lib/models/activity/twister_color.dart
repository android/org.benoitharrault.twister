import 'dart:math';

enum TwisterAllowedColors {
  blue,
  green,
  red,
  yellow,
}

class TwisterColor {
  final TwisterAllowedColors value;

  TwisterColor({
    required this.value,
  });

  factory TwisterColor.pickRandom() {
    int random = Random().nextInt(TwisterAllowedColors.values.length);
    return TwisterColor(value: TwisterAllowedColors.values[random]);
  }

  @override
  String toString() {
    List<String> parts = value.toString().split('.');
    return parts[1];
  }
}
